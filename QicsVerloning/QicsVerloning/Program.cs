﻿using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Excel = Microsoft.Office.Interop.Excel;

namespace QicsVerloning
{
    class Program
    {
        //Dit programma is snel in elkaar geknutseld als tijdelijke oplossing voor de verloning tussen loket.nl -> qics
        //Fonkel wil 1 factuur sturen naar de klant en niet deels via Flynth of in deel facturen, daarom moeten de facturen voor de loonverwerking wat via Flynth wordt gedaan in Qics worden geladen.
        //De prijs berekening is in beheer door Flynth zelf daarom kunnen wij het niet zelf genereren. = uiteindelijk wel het doel
        //Veel hardcoded stukken, maar goed, voor de snelheid is het niet dynamisch gemaakt
        //Robin Brunekreeft 24-07-2018
        static void Main(string[] args)
        {
            Console.WriteLine("Deze applicatie maakt op basis van een loket export de juiste import bestand voor Qics.");
            Console.WriteLine("In 'content/template.xlsx' staat de basis informatie van het te genereren bestand.");
            Console.WriteLine("In 'content/translatie.xlsx' staat alle basis klant informatie. Dit bestand linkt het loket bestand aan waardes die de Qics import nodig heeft!");
            Console.WriteLine("");

            //First lets ask what files to compare
            DirectoryInfo d = new DirectoryInfo("./");
            FileInfo[] fileEntries  = d.GetFiles("*.xlsm");

            bool inputCorrect = false;

            int fileNum = 0;

            while (!inputCorrect)
            {
                Console.WriteLine("Selecteer het loket input bestand:");

                for (int i = 0; i < fileEntries.Length; i++)
                    Console.WriteLine((i + 1) + ". " + fileEntries[i]);

                fileNum = Convert.ToInt32(Console.ReadLine()) - 1;

                if (fileNum >= 0 && fileNum < fileEntries.Length)
                    inputCorrect = true;
                else
                    Console.WriteLine("Geen geldig nummer, probeer opnieuw...");
            }

            //Second lets ask what month this file is about
            CultureInfo dutch   = new CultureInfo("nl-NL");
            inputCorrect        = false;
            int monthNum        = 0;

            while (!inputCorrect)
            {
                Console.WriteLine("Selecteer de factuur maand:");

                for(int i = 1; i < 13; i++)
                    Console.WriteLine(i + ". " + DateTime.Parse("2018-" + i + "-01").ToString("MMMM", dutch));

                monthNum = Convert.ToInt32(Console.ReadLine());

                if (monthNum >= 0 && monthNum < 13)
                    inputCorrect = true;
                else
                    Console.WriteLine("Geen geldig nummer, probeer opnieuw...");
            }

            //Lets load the content
            string[][] content = LoadContent("content/translatie.xlsx", "Sheet1");

            //Now that we loaded the content lets load the source from loket
            string[][] source = LoadContent(fileEntries[fileNum].ToString(), "Uitvoer");

            //Lets build the content of the file (combinging source and content)
            List<string[]> buildContent = new List<string[]>();

            Console.WriteLine();

            //Creating daytime format in dutch
            string monthYear = DateTime.Now.ToString("Y", dutch);

            //Listed lines
            List<int> linesUsed = new List<int>();

            //Totaal te factureren
            float totallPrice = 0;

            bool firstFound = false;

            for (int i = 1; i < content.Length; i++)
            {
                string sourceId = content[i][0].Trim();

                int sourceLine = -1;

                //Hard search for id in source
                for(int j = 1; j < source.Length; j++)
                {
                    //Extract the id from the "omschrijving"
                    /*
                    string toExtract = source[j][16];

                    string[] splitted = toExtract.Split(' ');

                    if (splitted.Length == 0 || !Regex.IsMatch(splitted[0], @"^\d+$"))
                        continue;
                    */

                    if (source[j][1] == sourceId)
                    {
                        sourceLine = j;

                        linesUsed.Add(sourceLine);

                        break;
                    }
                }

                if (sourceLine < 0)
                {
                    if (!firstFound)
                    {
                        Console.WriteLine("");
                        Console.WriteLine("De volgende regel(s) zijn wel opgenomen in het translatie bestand maar niet in het loket bestand:");

                        firstFound = true;
                    }

                    Console.WriteLine("De regel met ID " + sourceId + " is niet gevonden in het loket bestand. (Klant: " + content[i][1] + ")");
                    continue;
                }

                //Lets create the content line
                string[] buildLine = new string[42];

                //Hard coded building
                buildLine[1]    = content[i][6];       //Customer code
                //buildLine[3]    = content[i][2];       //Project
                buildLine[10]    = content[i][3];       //Project code
                //buildLine[5]    = content[i][4];       //Project name
                //buildLine[11]   = "Expense";           //Type
                //buildLine[12]   = "Approved";          //State
                buildLine[2]    = "Salarisverwerking " + DateTime.Parse("2018-" + monthNum + "-01").ToString("MMMM", dutch);   //Description
                buildLine[9]    = "14";
                buildLine[5]    = DateTime.Now.ToString("d-M-yyyy");
                buildLine[6]    = DateTime.Now.ToString("d-M-yyyy");
                buildLine[13]   = DateTime.Now.ToString("d-M-yyyy");


                if (content[i][4] == "Salarisverwerking" || content[i][4] == "Optie Salarisverwerking")
                {
                    buildLine[12] = "361";      //Item
                    //buildLine[19] = "361";      //Item code
                }
                else
                {
                    buildLine[12] = "362";      //Item
                    //buildLine[19] = "362";      //Item code
                }
                
                buildLine[15]   = "1";                         //Quantity

                //Check if its monthly or not
                if (content[i][5] == "maandelijks")
                    buildLine[16] = source[sourceLine][35];
                else
                {
                    //Only send factuur once a month
                    if (monthNum != 1)
                        buildLine[16] = "0";
                    else
                    {
                        float price = 0f;

                        buildLine[16] = Math.Ceiling(float.Parse(source[sourceLine][35]) * 12) + "";
                    }
                }

                try
                {
                    totallPrice += float.Parse(source[sourceLine][35]);
                }
                catch
                {
                    Console.WriteLine("Prijs in regel " + (sourceLine + 1) + " is incorrect.");
                }

                buildContent.Add(buildLine);
            }


            //Check which lines are not used from loket file
            firstFound = false;

            for (int i = 1; i < source.Length; i++)
            {
                /*
                string toExtract = source[i][1];

                string[] splitted = toExtract.Split(' ');

                if (splitted.Length == 0 || !Regex.IsMatch(splitted[0], @"^\d+$"))
                    continue;
                */

                //Check if this line is used
                if (!linesUsed.Contains<int>(i) && source[i][5].Contains("Fonkel"))
                {
                    if(!firstFound)
                    {
                        Console.WriteLine("");
                        Console.WriteLine("De volgende regel(s) zijn wel opgenomen in het loket bestand maar niet in het translatie bestand (obv naam SoortPakker die 'Fonkel' bevatten):");

                        firstFound = true;
                    }

                    Console.WriteLine("Regel " + (i + 1) + " met id " + source[i][1] + " komt niet voor in het translatie bestand.");
                }
                
            }

            //Show totall price
            Console.WriteLine("");
            Console.WriteLine("Totaal te factureren: " + totallPrice);

            //Lets start writing saving the file
            Console.WriteLine("");
            Console.WriteLine("Geef de bestandsnaam op (zonder .xlsx):");

            string fileName     = Console.ReadLine();
            string path         = "";

            using (ExcelPackage excel = new ExcelPackage(new FileInfo("content/invoices.xlsx")))
            {
                var sheet = excel.Workbook.Worksheets["Invoices"];

                //Adjust basic customer description
                //sheet.Cells[3, 3].Value = "Salarisverwerking " + DateTime.Parse("2018-" + monthNum + "-01").ToString("MMMM", dutch);
                //sheet.Cells[4, 3].Value = "Salarisverwerking " + DateTime.Parse("2018-" + monthNum + "-01").ToString("MMMM", dutch);

                //Lets add the content
                for (int i = 0; i < buildContent.Count; i++)
                {
                    for (int j = 0; j < buildContent[i].Length; j++)
                    {
                        if(j == 16)
                            sheet.Cells[i + 3, j + 1].Value = float.Parse(buildContent[i][j]);
                        //else if(j == 1 || j == 10 || j == 12 || j == 15)
                        //    sheet.Cells[i + 5, j + 1].Value = int.Parse(buildContent[i][j]);
                        //else if (j == 5 || j == 6 || j == 13)
                        //    sheet.Cells[i + 3, j + 1].Value = double.Parse(buildContent[i][j]);
                        else
                            sheet.Cells[i + 3, j + 1].Value = buildContent[i][j];
                    }
                }

                FileInfo excelFile = new FileInfo(fileName + ".xlsx");

                try
                {
                    excel.SaveAs(excelFile);
                }
                catch
                {
                    Console.WriteLine("Het is NIET gelukt om het bestand op te slaan. Het bestand is waarschijnlijk in gebruik.");
                    Console.ReadLine();
                    Environment.Exit(0);
                }
                

                path = excelFile.FullName;

                Console.WriteLine("Het bestand is aangemaakt: " + excelFile.FullName);
            }

            //Check if anything is skipped

            Console.WriteLine("");
            Console.WriteLine("Wil je het bestand direct openen? (Y/N)");
            string openFile = Console.ReadLine();

            if(openFile == "y" || openFile == "1" || openFile == "Y")
                System.Diagnostics.Process.Start(path);
        }

        public static string[][] LoadContent(string _fileName, string _sheetName = "")
        {
            Console.WriteLine("Bestand: " + _fileName + " laden!");

            string[][] content;

            FileInfo fileInfo = new FileInfo(_fileName);

            using (ExcelPackage excel = new ExcelPackage(fileInfo))
            {
                var sheet = excel.Workbook.Worksheets[_sheetName];

                var startSheet  = sheet.Dimension.Start;
                var endSheet    = sheet.Dimension.End;

                content = new string[endSheet.Row][];

                for (int row = startSheet.Row; row <= endSheet.Row; row++)
                {
                    content[row - 1] = new string[endSheet.Column];

                    for (int col = startSheet.Column; col <= endSheet.Column; col++)
                    {
                        object cellValue = sheet.Cells[row, col].Text; // This got me the actual value I needed.

                        content[row - 1][col - 1] = cellValue.ToString();
                    }
                }
            }

            if (content == null)
                Console.WriteLine("Failed to load file: " + _fileName);
            else
                Console.WriteLine("Bestand: " + _fileName + " geladen!");

            return content;
        }
    }
}
